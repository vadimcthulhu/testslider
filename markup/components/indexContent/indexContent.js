$(document).ready(function () {
    $('.s-slider').slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        variableWidth: true,
        responsive: [
            {
                breakpoint: 1280,
                settings: {
                    slidesToShow: 3,
                    variableWidth: false,
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    variableWidth: false,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    variableWidth: false
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    variableWidth: false
                }
            }
        ]
    }).on('mousewheel', function (event) {
        event.preventDefault();
        if (event.deltaX > 0 || event.deltaY < 0) {
            $(this).slick('slickNext');
        } else if (event.deltaX < 0 || event.deltaY > 0) {
            $(this).slick('slickPrev');
        }
    }).on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        if ( slick.slideCount === nextSlide + $(this).slick( 'slickGetOption', 'slidesToShow' ) ) {
            $(this).addClass('s-slider--last');
        } else {
            $(this).removeClass('s-slider--last');
        }

    });
});
